package com.example.danmed.servicesapp;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import com.example.danmed.servicesapp.entidades.ConSQLiteHelper;
import com.example.danmed.servicesapp.entidades.ConsultaResult;
import com.example.danmed.servicesapp.entidades.Project;
import com.example.danmed.servicesapp.utilidades.utilidades;

import java.util.ArrayList;

public class Index extends AppCompatActivity {

    private Button mButton;
    private TextView dialogtitle;
    final Context c = this;
    ListView lvprojects;
    ArrayList<String> InfoList;
    ArrayList<Project> ListProject;

    ArrayList<String> infoconsultaResult;
    ArrayList<ConsultaResult> ListConsultaResult;


    ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);



        dialogtitle= (TextView) findViewById(R.id.dialogTitle);
        lvprojects=(ListView)findViewById(R.id.lvprojects);
        Consultainicio();
       // consultar();
       // reloadadapter();
       // ConsultaResultmetodo();
//no funciona le metodo

        lvprojects.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posl, long l) {
                //final String category = "Position at : "+pos;

               //sijalo long cliikeable
                int idproject = ListProject.get(posl).getId_project();
                eliminarpregunta(idproject);

                return false;
            }
        });




/*public void eliminar(boolean valor)
        {
            if (valor){
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        }*/
        lvprojects.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l)
            {
             int idproject = ListProject.get(pos).getId_project();
             String nombreproject = ListProject.get(pos).getProject_Name();

                Intent intentprojecttosuite = new Intent(c,suiteClass.class);
                Bundle bundleprojecttosuites = new Bundle();

                bundleprojecttosuites.putString("nombreProject",nombreproject);
                bundleprojecttosuites.putInt("idProject",idproject);
                intentprojecttosuite.putExtras(bundleprojecttosuites);
               // Intent i = new Intent(getApplicationContext(),suiteClass.class);
                startActivity(intentprojecttosuite);


//aqui me envio el objeto a la siguiente lista de

/*  alert para eliminar proyecto
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                */
                //se crea siguente lista
              //  dialogtitle.setText("select any projec");
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                ""+nombreproject+""+String.valueOf(idproject)+"", Toast.LENGTH_SHORT);

                toast1.show();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);



//nada
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);
                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                final EditText userInputDialogEditText2 = (EditText) mView.findViewById(R.id.editText2);


                EditText nameProyject;


                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                AddProject(userInputDialogEditText.getText().toString());
                                consultar();
                                reloadadapter();

                                // ToDo get user input here
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

               // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                 //       .setAction("Action", null).show();
                //reloadadapter();
                userInputDialogEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                      //  userInputDialogEditText2.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }


        });

    }
    public  void Consultainicio() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Project project = null;
        int counta = 0;
        ListProject = new ArrayList<Project>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + utilidades.TABLA_PORJECT, null);
        //Cursor cursorcount = db.rawQuery("SELECT COUNT(*) FROM project", null);
        while (cursor.moveToNext()) {

            counta = cursor.getInt(0);

        }
        if (counta!=0) {

            //  if (cursor.moveToFirst()!=false);
            //{
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + utilidades.TABLA_PORJECT, null);
            while (cursor2.moveToNext())
            {
                project = new Project();
                project.setId_project(cursor2.getInt(0));
                project.setProject_Name(cursor2.getString(1));
                ListProject.add(project);
            }

            lvprojects.setVisibility(View.VISIBLE);

            getList();
            reloadadapter();
            //lvprojects.setVisibility(View.GONE);
        }
        else
            {
                lvprojects.setVisibility(View.GONE);
            }

    }

//    }
    private void eliminarpregunta(final int posle) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Toast toast1 =
                                Toast.makeText(getApplicationContext(),
                                        "elimaniar selecciono", Toast.LENGTH_SHORT);

                        toast1.show();
                       // int pos2=0;
                        //tengo que obtener la posision pero de id
                        int pos2 = posle;
                        ConsultaResultmetodo(pos2);
                        //si deseo eliminar
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage("Desea eliminar el proyecto de pruebas?").setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }



    private void reloadadapter() {

        //ArrayAdapter adapterlvw =  new ArrayAdapter(this,android.R.layout.simple_list_item_1,InfoList);
        ArrayAdapter adapterlvw =  new ArrayAdapter(this,android.R.layout.simple_list_item_1,InfoList);
        // /lvprojects.setAdapter(null);

        adapterlvw.notifyDataSetChanged();

        lvprojects.setAdapter(adapterlvw);
        //adapter.notifyDataSetChanged();
    }

//debo enviarle el id del proyecto
    private void ConsultaResultmetodo(int poslec) {

        //   ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        ConsultaResult consultaresult = null;
       // ListConsultaResult = new ArrayList<ConsultaResult>();
        boolean tiene3niveles= false;
        boolean tiene2niveles =false;
        Cursor cursor3 = db.rawQuery("select project.id_project, suite.id_suite,caset.id_case FROM  project,suite,caset where caset.fk_id_suite = suite.id_suite and suite.fk_id_project = project.id_project and project.id_project= "+poslec+"",null);
        while (cursor3.moveToNext())
        {
            consultaresult = new ConsultaResult();
            consultaresult.setId_project(cursor3.getInt(0));
            int idproject = cursor3.getInt(0);
            db.execSQL("delete from project where project.id_project="+idproject+"");
            consultaresult.setId_suite(cursor3.getInt(1));
            int idsuite = cursor3.getInt(1);
            db.execSQL("delete from suite where suite.id_suite="+idsuite+"");
            consultaresult.setId_case(cursor3.getInt(2));
            int idcase = cursor3.getInt(2); //cambie este 2 antes era 1
            db.execSQL("delete from caset where ="+idcase+"");
            //project.setProject_Name(cursor.getString(1));
            //ListConsultaResult.add(consultaresult);
            tiene3niveles=true;
        }

        if (tiene3niveles==false) {
            Cursor cursor4 = db.rawQuery("select project.id_project,suite.id_suite FROM  project,suite where  suite.fk_id_project = project.id_project and project.id_project=" + poslec + "", null);
            while (cursor4.moveToNext()) {
                consultaresult = new ConsultaResult();
                consultaresult.setId_project(cursor4.getInt(0));
                int idproject = cursor4.getInt(0);
                db.execSQL("delete from project where project.id_project=" + idproject + "");
                consultaresult.setId_suite(cursor4.getInt(1));
                int idsuite = cursor4.getInt(1);
                db.execSQL("delete from suite where suite.id_suite=" + idsuite + "");
               // consultaresult.setId_case(cursor4.getInt(2));
                //int idcase = cursor4.getInt(2);
                //db.execSQL("delete from caset where caset.fk_id_suite=" + idcase + "");
                //project.setProject_Name(cursor.getString(1));
                //ListConsultaResult.add(consultaresult);
                tiene2niveles=true;
            }
        }
        if (tiene2niveles==false)
        {
            Cursor cursor5 = db.rawQuery("select project.id_project FROM  project where  project.id_project=" + poslec + "", null);
            while (cursor5.moveToNext()) {
                consultaresult = new ConsultaResult();
                consultaresult.setId_project(cursor5.getInt(0));
                int idproject = cursor5.getInt(0);
                db.execSQL("delete from project where project.id_project=" + idproject + "");
               // consultaresult.setId_suite(cursor4.getInt(1));
                //int idsuite = cursor4.getInt(1);
                //db.execSQL("delete from suite where suite.fk_id_project=" + idsuite + "");
                // consultaresult.setId_case(cursor4.getInt(2));
                //int idcase = cursor4.getInt(2);
                //db.execSQL("delete from caset where caset.fk_id_suite=" + idcase + "");
                //project.setProject_Name(cursor.getString(1));
                //ListConsultaResult.add(consultaresult);
                //tiene2niveles=true;

            }
        }

          //  if()
            //{

            //}
            //saber si es el ultimo para eliminar vicivilidad


            Consultainicio();

       // deletereultadolist();

    }

    private void deletereultadolist() {
        SQLiteDatabase db = conn.getReadableDatabase();


        db.execSQL("delete from project where project.id_project=variableidsuite");
        db.execSQL("delete from suite where suite.id_suite=variableidsuite");
        db.execSQL("delete from caset where caset.id_case=variableidproyectlist");



    }


    private void consultar() {

     //   ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Project project = null;
        ListProject = new ArrayList<Project>();
        Cursor cursor = db.rawQuery("SELECT *FROM "+utilidades.TABLA_PORJECT,null);


                while (cursor.moveToNext())
                {
                    project = new Project();
                    project.setId_project(cursor.getInt(0));
                    project.setProject_Name(cursor.getString(1));
                    ListProject.add(project);
                }

                getList();
               // reloadadapter();




    }

    private void getList() {
        InfoList = new ArrayList<String>();
        for (int i = 0; i< ListProject.size(); i++)
        {
       // InfoList.add(String.valueOf(inttostring));
        InfoList.add(ListProject.get(i).getProject_Name());
        }
    }

    private void AddProject(String nameproject) {
       // ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn .getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(utilidades.PROJECT_NAME,nameproject);
        Long idresult = db.insert(utilidades.TABLA_PORJECT,utilidades.ID_PROJECT,values);
        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        ""+idresult+"", Toast.LENGTH_SHORT);

       // toast1.show();
        db.close();
      //  AddSuite("suite","1");
       // Addcase("case","1");
        lvprojects.setVisibility(View.VISIBLE);
    }
    private void AddSuite(String namesuite,String fk_id_project) {
      //  ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn .getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(utilidades.SUITE_NAME,namesuite);
        values.put(utilidades.FK_ID_PROJECT,fk_id_project);
        Long idresult = db.insert(utilidades.TABLA_SUITE,utilidades.ID_SUITE,values);
       // Toast toast1 =
         //       Toast.makeText(getApplicationContext(),
           //             ""+idresult+"", Toast.LENGTH_SHORT);

        // toast1.show();
        db.close();
    }

    private void Addcase(String namecase,String fk_id_suite) {
        //  ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn .getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(utilidades.CASE_NAME,namecase);
        values.put(utilidades.FK_ID_SUITE,fk_id_suite);
        Long idresult = db.insert(utilidades.TABLA_CASE,utilidades.ID_CASE,values);
       // Toast toast1 =
         //       Toast.makeText(getApplicationContext(),
           //             ""+idresult+"", Toast.LENGTH_SHORT);

        // toast1.show();
        db.close();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_index, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
