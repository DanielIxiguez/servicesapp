package com.example.danmed.servicesapp.entidades;

public class ConsultaResult {
    private int id_project;
    private int id_suite;
    private int id_case;

    public ConsultaResult() {
        this.id_project = id_project;
        this.id_suite = id_suite;
        this.id_case = id_case;
    }

    public int getId_project() {
        return id_project;
    }

    public void setId_project(int id_project) {
        this.id_project = id_project;
    }

    public int getId_suite() {
        return id_suite;
    }

    public void setId_suite(int id_suite) {
        this.id_suite = id_suite;
    }

    public int getId_case() {
        return id_case;
    }

    public void setId_case(int id_case) {
        this.id_case = id_case;
    }
}
