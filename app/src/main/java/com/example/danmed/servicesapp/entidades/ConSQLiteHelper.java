package com.example.danmed.servicesapp.entidades;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.danmed.servicesapp.utilidades.utilidades;

public class ConSQLiteHelper extends SQLiteOpenHelper {


    // final String CREATE_TABLE_SUITE="";
    //final String CREATE_TABLE_CASE="";
    public ConSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
         db.execSQL(utilidades.CREATE_TABLE_PROJET);
         db.execSQL(utilidades.CREATE_TABLE_SUITE);
         db.execSQL(utilidades.CREATE_TABLE_CASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       // if(oldVersion == 1 && newVersion >= 2){
         //   db.execSQL(db);
        //}
        db.execSQL("DROP TABLE IF EXISTS projets");
        db.execSQL("DROP TABLE IF EXISTS suite");
        db.execSQL("DROP TABLE IF EXISTS caset");
    onCreate(db);
    }
}
