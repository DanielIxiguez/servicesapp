package com.example.danmed.servicesapp;


 import android.content.ContentValues;
 import android.content.DialogInterface;
 import android.database.Cursor;
 import android.database.sqlite.SQLiteDatabase;
 import android.os.Bundle;
 import android.support.design.widget.FloatingActionButton;
 import android.support.design.widget.Snackbar;
 import android.support.v7.app.AlertDialog;
 import android.support.v7.app.AppCompatActivity;
 import android.support.v7.widget.Toolbar;
 import android.text.Editable;
 import android.text.TextWatcher;
 import android.view.LayoutInflater;
 import android.view.View;

 import android.content.Context;
 import android.widget.AdapterView;
 import android.widget.ArrayAdapter;
 import android.widget.EditText;
 import android.widget.ListView;
 import android.widget.Toast;

 import com.example.danmed.servicesapp.entidades.Case;
 import com.example.danmed.servicesapp.entidades.ConSQLiteHelper;
 import com.example.danmed.servicesapp.entidades.ConsultaResultCase;
 import com.example.danmed.servicesapp.entidades.ConsultaResultSuite;
 import com.example.danmed.servicesapp.entidades.Suite;
 import com.example.danmed.servicesapp.utilidades.utilidades;

 import java.util.ArrayList;


public class nettosuite extends AppCompatActivity {
    final Context c = this;
    ListView lvcases;
    ArrayList<String> InfoList;
    ArrayList<Case> Listcase;


    ConSQLiteHelper conn = new ConSQLiteHelper(this,"Test",null,1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lvcases=(ListView)findViewById(R.id.lvcase);
        int id_suite = recibirbundleFormSuite();
        ConsultainicioCase(id_suite);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.input_case, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);
                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                                int idSuite = recibirbundleFormSuite();
                                AddCase(userInputDialogEditText.getText().toString(),idSuite);
                                ConsultainicioCase(idSuite);
                                // ConsultainicioSuite(idSuite);
                        Toast toast1 = Toast.makeText(getApplicationContext(),"hola toast ", Toast.LENGTH_SHORT);

                                toast1.show();

                                // ToDo get user input here
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
                //reloadadapter();
                userInputDialogEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        //  userInputDialogEditText2.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        });
        lvcases.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posl, long l) {
                //final String category = "Position at : "+pos;

                //sijalo long cliikeable
                int idcase = Listcase.get(posl).getId_case();
                eliminarpregunta(idcase);


                return false;
            }
        });

 }


    private void eliminarpregunta(final int posle) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Toast toast1 =
                                Toast.makeText(getApplicationContext(),
                                        "elimaniar selecciono", Toast.LENGTH_SHORT);

                        toast1.show();
                        // int pos2=0;
                        //tengo que obtener la posision pero de id
                        int pos2 = posle;
                        ConsultaResultmetodo(pos2);
                        //si deseo eliminar
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage("Desea eliminar Lasuite de pruebas?").setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    private void ConsultaResultmetodo(int poslec)
    {

        //   ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        ConsultaResultCase consultaresultcase = null;

        // ListConsultaResult = new ArrayList<ConsultaResult>();
        Cursor cursor4 = db.rawQuery("select * from caset where caset.id_case =" + poslec + "", null);
        while (cursor4.moveToNext()) {
            consultaresultcase = new ConsultaResultCase();
            consultaresultcase.setId_case(cursor4.getInt(0));
            int idcase = cursor4.getInt(0);
            db.execSQL("delete from caset where caset.id_case=" + idcase + "");
        }
        int idcase = recibirbundleFormSuite();
        ConsultainicioCase(idcase);
        }



    private void AddCase(String name_case, int fk_id_suite)
    {
        SQLiteDatabase db = conn .getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(utilidades.CASE_NAME,name_case);
        values.put(utilidades.FK_ID_SUITE,fk_id_suite);
        Long idresult = db.insert(utilidades.TABLA_CASE,utilidades.ID_CASE,values);

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        ""+idresult+"", Toast.LENGTH_SHORT);
        toast1.show();
        db.close();
       // lvcase.setVisibility(View.VISIBLE);
    }

    private int recibirbundleFormSuite() {

        Bundle bundleFromsuite = this.getIntent().getExtras();
        if (bundleFromsuite!=null)
        {
            String Namesuite = bundleFromsuite.getString("nombreSuite");
            int IdSuite = bundleFromsuite.getInt("idSuite");
            return IdSuite;
            //Toast toast1 =
            //      Toast.makeText(getApplicationContext(),
            //            ""+NameProject+""+String.valueOf(IdProject)+"", Toast.LENGTH_SHORT);

            //toast1.show();
        }
        return 0;
    }

    private void ConsultainicioCase(int fk_id_suite)
    {
        SQLiteDatabase db = conn.getReadableDatabase();
        Case casel = null;
        int counta = 0;
        Listcase = new ArrayList<Case>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + utilidades.TABLA_CASE +" WHERE " + utilidades.FK_ID_SUITE+utilidades.IGUAL+fk_id_suite, null);
        //Cursor cursorcount = db.rawQuery("SELECT COUNT(*) FROM suite", null);
        while (cursor.moveToNext()) {

            counta = cursor.getInt(0);

        }
        if (counta!=0) {

            //  if (cursor.moveToFirst()!=false);
            //{
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + utilidades.TABLA_CASE+" WHERE " + utilidades.FK_ID_SUITE+utilidades.IGUAL+fk_id_suite, null);
            while (cursor2.moveToNext())
            {
                casel = new Case();
                casel.setId_case(cursor2.getInt(0));
                casel.setCase_name(cursor2.getString(1));
                casel.setFk_id_suite(cursor2.getInt(2));
                Listcase.add(casel);
            }

            lvcases.setVisibility(View.VISIBLE);
            getList();
            reloadadapter();
            /* si va pero aun no tengo metodos


             */

            // esta no /// lvprojects.setVisibility(View.GONE);
        }
        else
        {
            lvcases.setVisibility(View.GONE);
        }


    }
    private void getList() {
        InfoList = new ArrayList<String>();
        for (int i = 0; i< Listcase.size(); i++)
        {
            // InfoList.add(String.valueOf(inttostring));
            InfoList.add(Listcase.get(i).getCase_name());
        }
    }
    private void reloadadapter() {

        ArrayAdapter adapter =  new ArrayAdapter(c,android.R.layout.simple_list_item_1,InfoList);



        lvcases.setAdapter(adapter);

    }



}
