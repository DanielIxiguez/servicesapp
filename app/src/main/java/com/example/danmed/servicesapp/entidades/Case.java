package com.example.danmed.servicesapp.entidades;

public class Case {
    private int id_case;
    private String case_name;
    private int fk_id_suite;

    public Case() {
        this.id_case = id_case;
        this.case_name = case_name;
        this.fk_id_suite = fk_id_suite;
    }

    public int getId_case() {
        return id_case;
    }

    public void setId_case(int id_case) {
        this.id_case = id_case;
    }

    public String getCase_name() {
        return case_name;
    }

    public void setCase_name(String case_name) {
        this.case_name = case_name;
    }

    public int getFk_id_suite() {
        return fk_id_suite;
    }

    public void setFk_id_suite(int fk_id_suite) {
        this.fk_id_suite = fk_id_suite;
    }
}
