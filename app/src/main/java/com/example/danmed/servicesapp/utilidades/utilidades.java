package com.example.danmed.servicesapp.utilidades;

public class utilidades {
    public static final String TABLA_PORJECT = "project";
    public static final String TABLA_SUITE = "suite";
    public static final String TABLA_CASE = "caset";

    public static final String ID_PROJECT  = "id_project";
    public static final String ID_CASE  = "id_case";
    public static final String ID_SUITE  = "id_suite";


    public static final String PROJECT_NAME = "project_name";
    public static final String SUITE_NAME= "suite_name";
    public static final String CASE_NAME = "case_name";

    public static final String FK_ID_PROJECT = "fk_id_project";
    public static final String FK_ID_SUITE= "fk_id_suite";
    public  static  final String IGUAL = "=";
    //public final String  = "";
    //public final String  = "";

    public static final String CREATE_TABLE_PROJET="CREATE TABLE "+TABLA_PORJECT+" ("+ID_PROJECT+" INTEGER PRIMARY KEY   AUTOINCREMENT,"+PROJECT_NAME+" TEXT NOT NULL)";
    public static final String CREATE_TABLE_SUITE="CREATE TABLE "+TABLA_SUITE+" ("+ID_SUITE+" INTEGER PRIMARY KEY   AUTOINCREMENT,"+SUITE_NAME+" TEXT NOT NULL,"+FK_ID_PROJECT+" TEXT NOT NULL)";
    public static final String CREATE_TABLE_CASE="CREATE TABLE "+TABLA_CASE+" ("+ID_CASE+" INTEGER PRIMARY KEY   AUTOINCREMENT,"+CASE_NAME+" TEXT NOT NULL,"+FK_ID_SUITE+" TEXT NOT NULL)";
}
