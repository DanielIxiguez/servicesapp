package com.example.danmed.servicesapp.entidades;

public class Suite {
private int id_suite;
private String suite_name;
private int fk_id_projet;

    public Suite() {
        this.id_suite = id_suite;
        this.suite_name = suite_name;
        this.fk_id_projet = fk_id_projet;
    }

    public int getId_suite() {
        return id_suite;
    }

    public void setId_suite(int id_suite) {
        this.id_suite = id_suite;
    }

    public String getSuite_name() {
        return suite_name;
    }

    public void setSuite_name(String suite_name) {
        this.suite_name = suite_name;
    }

    public int getFk_id_projet() {
        return fk_id_projet;
    }

    public void setFk_id_projet(int fk_id_projet) {
        this.fk_id_projet = fk_id_projet;
    }
}
