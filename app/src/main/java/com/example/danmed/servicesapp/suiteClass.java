package com.example.danmed.servicesapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.example.danmed.servicesapp.entidades.ConSQLiteHelper;
import com.example.danmed.servicesapp.entidades.ConsultaResultSuite;
import com.example.danmed.servicesapp.entidades.Suite;
import com.example.danmed.servicesapp.utilidades.utilidades;

import java.util.ArrayList;

public class suiteClass extends AppCompatActivity {
    final Context c = this;
    ListView lvsuites;
    ArrayList<String> InfoList;
    ArrayList<Suite> Listsuites;

    ArrayList<String> infoconsultaResult;

    //ArrayList<ConsultaResultSuite> ListConsultaResult;

    ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suite_class);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lvsuites=(ListView)findViewById(R.id.lvsuite);
        int idProject = recibirbundleFormProject();

        ConsultainicioSuite(idProject);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.input_suite, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                alertDialogBuilderUserInput.setView(mView);
                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                                int idProject = recibirbundleFormProject();
                                AddSuite(userInputDialogEditText.getText().toString(),idProject);

                                ConsultainicioSuite(idProject);

                                // consultar();
                                 //reloadadapter();

                                // ToDo get user input here
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
                //reloadadapter();
                userInputDialogEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        //  userInputDialogEditText2.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                //    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //          .setAction("Action", null).show();
            }
        });

        lvsuites.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l)
            {
                int idsuite = Listsuites.get(pos).getId_suite();
                String nombresuite = Listsuites.get(pos).getSuite_name();

                Intent intentsuitetocase = new Intent(c,nettosuite.class);
                Bundle bundlesuitetocase = new Bundle();

                bundlesuitetocase.putString("nombreSuite",nombresuite);
                bundlesuitetocase.putInt("idSuite",idsuite);
                intentsuitetocase.putExtras(bundlesuitetocase);
                // Intent i = new Intent(getApplicationContext(),suiteClass.class);
                startActivity(intentsuitetocase);

               // Toast toast1 =
                 //       Toast.makeText(getApplicationContext(),
                   ///             ""+nombresuite+""+String.valueOf(idsuite)+"", Toast.LENGTH_SHORT);

                //toast1.show();
            }
        });

        lvsuites.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posl, long l) {
                //final String category = "Position at : "+pos;

                //sijalo long cliikeable
                int idsuite = Listsuites.get(posl).getId_suite();
                eliminarpregunta(idsuite);


                return false;
            }
        });

    }

    private void eliminarpregunta(final int posle) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Toast toast1 =
                                Toast.makeText(getApplicationContext(),
                                        "elimaniar selecciono", Toast.LENGTH_SHORT);

                        toast1.show();
                        // int pos2=0;
                        //tengo que obtener la posision pero de id
                        int pos2 = posle;
                        ConsultaResultmetodo(pos2);
                        //si deseo eliminar
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage("Desea eliminar Lasuite de pruebas?").setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void ConsultaResultmetodo(int poslec)
    {

        //   ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        ConsultaResultSuite consultaresultsuite = null;
        // ListConsultaResult = new ArrayList<ConsultaResult>();
        boolean tiene3niveles= false;
        boolean tiene2niveles =false;
        Cursor cursor3 = db.rawQuery("select suite.id_suite,caset.id_case FROM  suite,caset where caset.fk_id_suite = suite.id_suite and suite.id_suite= "+poslec+"",null);
        while (cursor3.moveToNext())
        {
            consultaresultsuite = new ConsultaResultSuite();
            consultaresultsuite.setId_suite(cursor3.getInt(0));
            int idsuite = cursor3.getInt(0);
            db.execSQL("delete from suite where suite.id_suite="+idsuite+"");

            consultaresultsuite.setId_case(cursor3.getInt(1));
            int idcase = cursor3.getInt(1);
            db.execSQL("delete FROM  caset where  caset.id_case="+idcase+"");
            //consultaresultsuite.setId_case(cursor3.getInt(2));
            //int idcase = cursor3.getInt(2); //cambie este 2 antes era 1
            //db.execSQL("delete from caset where caset.fk_id_suite="+idcase+"");
            //project.setProject_Name(cursor.getString(1));
            //ListConsultaResult.add(consultaresultsuite);
            tiene2niveles=true;
        }

        if (tiene2niveles==false) {
            Cursor cursor4 = db.rawQuery("select * from suite where suite.id_suite=" + poslec + "", null);
            while (cursor4.moveToNext()) {
                consultaresultsuite = new ConsultaResultSuite();
                consultaresultsuite.setId_suite(cursor4.getInt(0));
                int idsuite = cursor4.getInt(0);
                db.execSQL("delete from suite where suite.id_suite=" + idsuite + "");
            }
        }
        int idProject = recibirbundleFormProject();
        ConsultainicioSuite(idProject);

        // deletereultadolist();

    }

    private void consultar()
    {

        //   ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Suite suite = null;
        Listsuites = new ArrayList<Suite>();
        Cursor cursor = db.rawQuery("SELECT *FROM "+utilidades.TABLA_PORJECT,null);


        while (cursor.moveToNext())
        {
            suite = new Suite();
            suite.setId_suite(cursor.getInt(0));
            suite.setSuite_name(cursor.getString(1));
            Listsuites.add(suite);
        }

        getList();
        // reloadadapter();


        while (cursor.moveToNext())
        {
            suite = new Suite();
            suite.setId_suite(cursor.getInt(0));
            suite.setSuite_name(cursor.getString(1));
            Listsuites.add(suite);
        }

        getList();
    }


    private void ConsultainicioSuite(int fk_id_project)
    {
        SQLiteDatabase db = conn.getReadableDatabase();
        Suite suite = null;
        int counta = 0;
        Listsuites = new ArrayList<Suite>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + utilidades.TABLA_SUITE +" WHERE " + utilidades.FK_ID_PROJECT+utilidades.IGUAL+fk_id_project, null);
        //Cursor cursorcount = db.rawQuery("SELECT COUNT(*) FROM suite", null);
        while (cursor.moveToNext()) {

            counta = cursor.getInt(0);

        }
        if (counta!=0) {

            //  if (cursor.moveToFirst()!=false);
            //{
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + utilidades.TABLA_SUITE +" WHERE " + utilidades.FK_ID_PROJECT+utilidades.IGUAL+fk_id_project, null);
            while (cursor2.moveToNext())
            {
                suite = new Suite();
                suite.setId_suite(cursor2.getInt(0));
                suite.setSuite_name(cursor2.getString(1));
                suite.setFk_id_projet(cursor2.getInt(2));
                Listsuites.add(suite);
            }

            lvsuites.setVisibility(View.VISIBLE);
            getList();
            reloadadapter();
/* si va pero aun no tengo metodos


            */

            // esta no /// lvprojects.setVisibility(View.GONE);
        }
        else
        {
             lvsuites.setVisibility(View.GONE);
        }


    }

    private void reloadadapter() {

        ArrayAdapter adapterlvws =  new ArrayAdapter(c,android.R.layout.simple_list_item_1,InfoList);
        //ArrayAdapter adapterlvw =  new ArrayAdapter(c,InfoList);
        //lvprojects.setAdapter(null);

        adapterlvws.notifyDataSetChanged();

        lvsuites.setAdapter(adapterlvws);
        //adapter.notifyDataSetChanged();
    }
    private void getList() {
        InfoList = new ArrayList<String>();
        for (int i = 0; i< Listsuites.size(); i++)
        {
            // InfoList.add(String.valueOf(inttostring));
            InfoList.add(Listsuites.get(i).getSuite_name());
        }
    }

    private int recibirbundleFormProject() {

        Bundle bundleFromProjects = this.getIntent().getExtras();
        if (bundleFromProjects!=null)
        {
            String NameProject = bundleFromProjects.getString("nombreProject");
            int IdProject = bundleFromProjects.getInt("idProject");
            return IdProject;
            //Toast toast1 =
            //      Toast.makeText(getApplicationContext(),
            //            ""+NameProject+""+String.valueOf(IdProject)+"", Toast.LENGTH_SHORT);

            //toast1.show();
        }
        return 0;
    }

    private void AddSuite(String nameSuite,int id_project) {



        // ConSQLiteHelper  conn = new ConSQLiteHelper(this,"Test",null,1);
        SQLiteDatabase db = conn .getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(utilidades.SUITE_NAME,nameSuite);
        values.put(utilidades.FK_ID_PROJECT,id_project);
        Long idresult = db.insert(utilidades.TABLA_SUITE,utilidades.ID_SUITE,values);

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        ""+idresult+"", Toast.LENGTH_SHORT);

        toast1.show();
        db.close();
        // AddSuite("suite","1");
        // Addcase("case","1");

        // si av este linea pero me da error
        lvsuites.setVisibility(View.VISIBLE);
    }

}
